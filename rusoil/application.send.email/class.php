<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\CurrentUser;

class RusOilApplicationSendEmailComponent extends \CBitrixComponent
{
    private const EMAIL = 'exemple@mail.ru';

    public function onPrepareComponentParams($params)
    {
        return $params;
    }

    public function executeComponent()
    {
        try {
            $request = $this->request;
            if ($request->get('form-submit')) {
                $this->sendData();
            }

            $this->arResult = array(
                'FIELDS' => self::getFields(),
            );
        } catch (\Throwable $exception) {
            $this->arResult = array(
                'ERROR' => $exception->getMessage(),
            );
        }

        $this->includeComponentTemplate();
    }

    private function sendData(): void
    {
        $to = self::EMAIL;
        $subject = Loc::getMessage('MAIL_SUBJECT');
        $body = $this->getMessageText();

        $from = CurrentUser::get()->getEmail();

        $boundary = md5(uniqid(time()));

        // Формирование заголовка письма.
        $headers = array(
            'Content-Type: multipart/mixed; boundary="' . $boundary . '"',
            'Content-Transfer-Encoding: 7bit',
            'MIME-Version: 1.0',
            'From: ' . $from,
            'Date: ' . date('r')
        );

        // Формирование текста письма.
        $message = array(
            '--' . $boundary,
            'Content-Type: text/html; charset=UTF-8',
            '',
            $body
        );

        // Прикрепление файлов
        $fileFields = self::getFieldsByType('file');

        foreach ($fileFields as $fileField) {
            if ($fileField['multiple']) {
                $fileArray = array();

                for ($i = 0; $i < count($_FILES[$fileField['id']]['name']); $i++) {
                    $fileArray[] = array(
                        'name' => $_FILES[$fileField['id']]['name'][$i],
                        'tmp_name' => $_FILES[$fileField['id']]['tmp_name'][$i],
                        'size' => $_FILES[$fileField['id']]['size'][$i],
                        'type' => $_FILES[$fileField['id']]['type'][$i],
                    );
                }

                foreach ($fileArray as $file) {
                    $message = array_merge($message, $this->getFileForMail($file, $boundary));
                }
            } else {
                $message = array_merge($message, $this->getFileForMail($_FILES[$fileField['id']], $boundary));
            }
        }

        $message[] = '';
        $message[] = "--" . $boundary . '--';

        $headers = implode("\r\n", $headers);
        $message = implode("\r\n", $message);

        mail($to, $subject, $message, $headers);
    }

    private function getFileForMail(array $fileData, string $boundary): array
    {
        if (!is_uploaded_file($fileData['tmp_name'])) {
            return array();
        }

        $message = array();

        $name = $fileData['name'];
        $fp   = fopen($fileData['tmp_name'], 'rb');
        $file = fread($fp, $fileData['size']);
        fclose($fp);

        $message[] =  '';
        $message[] =  '--' . $boundary;
        $message[] = 'Content-Type: ' . $fileData['type'] . '; name="' . $name . '"';
        $message[] = 'Content-Transfer-Encoding: base64';
        $message[] = 'Content-Disposition: attachment; filename="' . $name . '"';
        $message[] = '';
        $message[] = chunk_split(base64_encode($file));

        return $message;
    }

    private function getMessageText(): string
    {
        $result = '';

        $fields = self::getFields();

        foreach ($fields as $field) {
            if (empty($this->request->get($field['id']))) {
                continue;
            }
            if ($field['type'] === 'file' || $field['type'] === 'custom_rows') {
                continue;
            }

            $result .= (!empty($field['title']) ? $field['title'] : $field['id']) . ' - ' . $this->request->get($field['id']) . PHP_EOL;
        }

        $customRows = $this->getCustomRowsData();

        foreach ($customRows as $fieldId => $rows) {
            $field = self::getField($fieldId);

            if (empty($field)) {
                continue;
            }

            $result .= (!empty($field['title']) ? $field['title'] : $field['id']) . ':' . PHP_EOL;

            foreach ($rows as $i => $row) {
                $result .= $i + 1 . '.' . PHP_EOL;

                foreach ($row as $propertyName => $propertyValue) {
                    $result .= $propertyName . ' - ' . $propertyValue . PHP_EOL;
                }
            }
        }

        return $result;
    }

    private static function getField(string $value, string $key = 'id'): array|null
    {
        foreach (self::getFields() as $field) {
            if ($field[$key] === $value) {
                return $field;
            }
        }

        return null;
    }


    private function getCustomRowsData(): array
    {
        $result = array();

        $customRowsFields = self::getFieldsByType('custom_rows');

        foreach ($customRowsFields as $field) {
            $countRow = count($this->request->get($field['properties'][0]['id']));

            for ($i = 0; $i < $countRow; $i++) {
                foreach ($field['properties'] as $property) {
                    $result[$field['id']][$i][$property['name']] = $this->request->get($property['id'])[$i];
                }
            }
        }

        return $result;
    }

    private static function getFieldsByType(string $type): array
    {
        $result = array();

        foreach (self::getFields() as $field) {
            if ($field['type'] !== $type) {
                continue;
            }

            $result[] = $field;
        }

        return $result;
    }

    public static function getFields(): array
    {
        return array(
            array(
                'id' => 'title',
                'title' => Loc::getMessage('FIELDS_TITLE_TITLE'),
                'type' => 'text',
                'required' => true,
            ),
            array(
                'id' => 'category',
                'title' => Loc::getMessage('FIELDS_CATEGORY_TITLE'),
                'type' => 'radio',
                'items' => array(
                    Loc::getMessage('FIELDS_CATEGORY_ITEM_1'),
                    Loc::getMessage('FIELDS_CATEGORY_ITEM_2'),
                ),
                'required' => true,
            ),
            array(
                'id' => 'type',
                'title' => Loc::getMessage('FIELDS_TYPE_TITLE'),
                'type' => 'radio',
                'items' => array(
                    Loc::getMessage('FIELDS_TYPE_ITEM_1'),
                    Loc::getMessage('FIELDS_TYPE_ITEM_2'),
                    Loc::getMessage('FIELDS_TYPE_ITEM_3'),
                ),
                'required' => true,
            ),
            array(
                'id' => 'storage',
                'title' => Loc::getMessage('FIELDS_STORAGE_TITLE'),
                'type' => 'list',
                'items' => array(
                    Loc::getMessage('FIELDS_STORAGE_ITEM_1'),
                    Loc::getMessage('FIELDS_STORAGE_ITEM_2'),
                ),
            ),
            array(
                'id' => 'rows',
                'title' => Loc::getMessage('FIELDS_ROWS_TITLE'),
                'type' => 'custom_rows',
                'properties' => array(
                    array(
                        'id' => 'brand',
                        'name' => Loc::getMessage('FIELDS_ROWS_ROW_BRAND'),
                        'type' => 'list',
                        'items' => array(
                            Loc::getMessage('ROW_BRAND_1') => Loc::getMessage('ROW_BRAND_1'),
                            Loc::getMessage('ROW_BRAND_2') => Loc::getMessage('ROW_BRAND_2'),
                            Loc::getMessage('ROW_BRAND_3') => Loc::getMessage('ROW_BRAND_3'),
                        )
                    ),
                    array(
                        'id' => 'name',
                        'name' => Loc::getMessage('FIELDS_ROWS_ROW_NAME'),
                        'type' => 'text'
                    ),
                    array(
                        'id' => 'count',
                        'name' => Loc::getMessage('FIELDS_ROWS_ROW_COUNT'),
                        'type' => 'number'
                    ),
                    array(
                        'id' => 'packing',
                        'name' => Loc::getMessage('FIELDS_ROWS_ROW_PACKING'),
                        'type' => 'text'
                    ),
                    array(
                        'id' => 'client',
                        'name' => Loc::getMessage('FIELDS_ROWS_ROW_CLIENT'),
                        'type' => 'text'
                    ),
                ),
            ),
            array(
                'id' => 'file',
                'title' => Loc::getMessage('FIELDS_FILE_TITLE'),
                'type' => 'file',
                'multiple' => true,
            ),
            array(
                'id' => 'comment',
                'title' => Loc::getMessage('FIELDS_COMMENT_TITLE'),
                'type' => 'textarea',
            ),
        );
    }
}