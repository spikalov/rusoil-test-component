<?php
require __DIR__ . '/class.php';

use Bitrix\Main\Engine\Controller;

class RusOilApplicationSendEmailAjaxComponent extends Controller
{
    public function getRequiredFieldsAction(): array
    {
        $fields = RusOilApplicationSendEmailComponent::getFields();

        $result = array();

        foreach ($fields as $field) {
            if ($field['required'] === true) {
                $result[] = $field['id'];
            }
        }

        return $result;
    }
}