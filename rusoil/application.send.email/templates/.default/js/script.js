BX.ready(function () {
    //custom-row region
    // Добавление строки
    const addRowButtons = document.querySelectorAll('.addRow');
    for (let index = 0; index < addRowButtons.length; index++) {
        let button = addRowButtons[index];

        button.addEventListener('click', event => {
            let properties = event.composedPath()[2].querySelector('.custom-row__properties');
            let newRow = properties.cloneNode(true);
            let btnBox = event.composedPath()[1];

            for (let input of newRow.querySelectorAll('input')) {
                input.value = null;
            }

            let removeBtn = btnBox.querySelector('.removeRow');
            removeBtn.classList.remove('ui-btn-disabled');

            btnBox.before(newRow);
        });
    }

    // Удаление строки
    const removeRowButtons = document.querySelectorAll('.removeRow');
    for (let index = 0; index < removeRowButtons.length; index++) {
        let button = removeRowButtons[index];

        button.addEventListener('click', event => {
            let properties = event.composedPath()[2].querySelectorAll('.custom-row__properties');

            if (properties.length === 2) {
                let btnBox = event.composedPath()[1];
                let removeBtn = btnBox.querySelector('.removeRow');

                removeBtn.classList.add('ui-btn-disabled');
            }

            let lastRow = properties[properties.length - 1];
            lastRow.remove();
        });
    }
    //endregion

    //submit region
    const submitBtn = document.getElementById('submit-btn');
    submitBtn.addEventListener('click', event => {
        submitBtn.classList.add('ui-btn-clock');

        BX.ajax.runComponentAction(
            'rusoil:application.send.email',
            'getRequiredFields',
            {
                mode: 'ajax'
            }
        ).then(function (response) {
            if (!checkRequiredFields(response.data)) {
                throw 'Не заполнены обязательные поля';
            }

            let form = document.getElementById('form');
            form.submit();
        }).catch(function (error) {
            submitBtn.classList.remove('ui-btn-clock');
            console.log(error);
        });
    });

    /**
     * Проверка заполнения обязательных полей
     *
     * @param requiredFields
     * @returns {boolean}
     */
    function checkRequiredFields(requiredFields) {
        let result = true;

        for (let requiredField of requiredFields) {
            let field = document.getElementById(requiredField);
            let inputs = field.querySelectorAll('[name="' + requiredField + '"]');

            let fieldWrite = false;

            for (let input of inputs) {
                if (input.type === 'radio' && input.checked) {
                    field.classList.remove('border');
                    field.classList.remove('border-danger');

                    fieldWrite = true;
                    break;
                }
                if (input.type !== 'radio' && input.value.length > 0) {
                    field.classList.remove('border');
                    field.classList.remove('border-danger');

                    fieldWrite = true;
                    break;
                }

                field.classList.add('border');
                field.classList.add('border-danger');
            }

            if (!fieldWrite) {
                result = false;
            }
        }

        return result;
    }
    //endregion
});