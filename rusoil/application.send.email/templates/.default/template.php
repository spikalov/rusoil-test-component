<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\UI\Extension;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

/** @var array $arParams */
/** @var array $arResult */
/** @global \CMain $APPLICATION */
/** @global \CUser $USER */
/** @global \CDatabase $DB */
/** @var \CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var array $templateData */
/** @var \CBitrixComponent $component */

Extension::load('ui.bootstrap4');
Extension::load('ui.buttons');

Asset::getInstance()->addJs($templateFolder . '/js/script.js');

if (!empty($arResult['ERROR'])) {
    $APPLICATION->ThrowException($arResult['ERROR']);
    ShowError($arResult['ERROR']);
    return;
}
$APPLICATION->SetTitle(Loc::getMessage('PAGE_TITLE'));
?>

<div class="container-fluid">
    <form enctype="multipart/form-data" class="card p-3" id="form" action="#" method="post">
        <input type="hidden" name="form-submit" value="true">
        <?php foreach ($arResult['FIELDS'] as $field):
                if (empty($field['type']) || empty($field['id'])) {
                    continue;
                }
        ?>
            <div class="field m-1 p-1 rounded" id="<?= $field['id'] ?>">

                <!-- Название поля -->
                <div class="d-flex">
                    <?php if (!empty($field['title'])): ?>
                        <h5 class="card-title"><?= $field['title'] ?></h5>
                    <?php endif; ?>
                    <?php if ($field['required']): ?>
                        <span class="text-danger">*</span>
                    <?php endif; ?>
                </div>
                <!-- /Название поля -->

                <!-- Описание поля -->
                <?php if (!empty($field['description'])): ?>
                    <h6 class="card-subtitle  mb-2 text-secondary"><?= $field['description'] ?></h6>
                <?php endif; ?>
                <!-- /Описание поля -->

                <!-- Кастомные строки -->
                <?php if ($field['type'] === 'custom_rows'): ?>
                    <div class="custom-row">
                        <div class="d-flex custom-row__properties">
                            <?php
                            foreach ($field['properties'] as $property):
                                if (empty($property['id']) || empty($property['type'])){
                                    continue;
                                }
                                ?>
                                <div class="property mx-1">
                                    <?php if (!empty($property['name'])): ?>
                                        <div class="property-name text-center"><?= $property['name'] ?></div>
                                    <?php endif; ?>
                                    <?php if ($property['type'] === 'text'): ?>
                                        <input type="text" name="<?= $property['id'] ?>[]" class="form-control">
                                    <?php endif; ?>
                                    <?php if ($property['type'] === 'number'): ?>
                                        <input type="number" name="<?= $property['id'] ?>[]" class="form-control">
                                    <?php endif; ?>
                                    <?php if ($property['type'] === 'list'): ?>
                                        <select class="form-select" name="<?= $property['id'] ?>[]">
                                            <option selected><?= Loc::getMessage('FIELD_SELECT_DEFAULT') ?></option>
                                            <?php foreach ($property['items'] as $item): ?>
                                                <option value="<?= $item ?>"><?= $item ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="custom-rows__btns d-flex p-1">
                            <button type="button" class="ui-btn ui-btn-icon-add ui-btn-success ui-btn-round addRow"></button>
                            <button type="button" class="ui-btn ui-btn-icon-remove ui-btn-danger ui-btn-round removeRow ui-btn-disabled "></button>
                        </div>
                    </div>
                <!-- /Кастомные строки -->

                <!-- Текст -->
                <?php elseif ($field['type'] === 'text'): ?>
                    <input type="text" name="<?= $field['id'] ?>" class="form-control">
                <!-- /Текст -->

                <!-- Радиокнопки -->
                <?php elseif ($field['type'] === 'radio'): ?>
                    <?php foreach ($field['items'] as $i => $item): ?>
                        <div class="form-check">
                            <input type="radio" id="<?= $field['id'] . $i ?>" name="<?= $field['id'] ?>" value="<?= $item ?>" class="form-check-input">
                            <label class="form-check-label" for="<?= $field['id'] . $i ?>">
                                <?= $item ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                <!-- /Радиокнопки -->

                <!-- Список -->
                <?php elseif ($field['type'] === 'list'): ?>
                    <select class="form-select" name="<?= $field['id'] ?>">
                        <option selected><?= Loc::getMessage('FIELD_SELECT_DEFAULT') ?></option>
                        <?php foreach ($field['items'] as $i => $item): ?>
                            <option value="<?= $item ?>"><?= $item ?></option>
                        <?php endforeach; ?>
                    </select>
                <!-- /Список -->

                <!-- Поле для ввода текста -->
                <?php elseif ($field['type'] === 'textarea'): ?>
                    <textarea name="<?= $field['id'] ?>" class="form-control" rows="3"></textarea>
                <!-- /Поле для ввода текста -->

                <!-- Файлы -->
                <?php elseif ($field['type'] === 'file'): ?>
                    <input type="file" name="<?= $field['id'] . ($field['multiple'] ? '[]' : '' )?>" <?= $field['multiple'] ? 'multiple' : '' ?>>
                <!-- /Файлы -->
                <?php endif; ?>
            </div>
        <?php
            unset($field);
            endforeach;
        ?>
        <div class="submit-btn mx-auto">
            <button type="button" class="ui-btn ui-btn-primary" id="submit-btn"><?= Loc::getMessage('SUBMIT_BTN_TEXT') ?></button>
        </div>
    </form>
</div>
