<?php
$MESS ["FIELDS_TITLE_TITLE"] = "Заголовок заявки";

$MESS ["FIELDS_CATEGORY_TITLE"] = "Категория";
$MESS ["FIELDS_CATEGORY_ITEM_1"] = "Масла, автохимия, Автоаксессуары, обогреватели, запчасти, сопутствующие товары";
$MESS ["FIELDS_CATEGORY_ITEM_2"] = "Шины, диски";

$MESS ["FIELDS_TYPE_TITLE"] = "Вид заявки";
$MESS ["FIELDS_TYPE_ITEM_1"] = "Запрос цены и сроков поставки";
$MESS ["FIELDS_TYPE_ITEM_2"] = "Пополнение складов";
$MESS ["FIELDS_TYPE_ITEM_3"] = "Спецзаказ";

$MESS ["FIELDS_STORAGE_TITLE"] = "Склад поставки";
$MESS ["FIELDS_STORAGE_ITEM_1"] = "Склад 1";
$MESS ["FIELDS_STORAGE_ITEM_2"] = "Склад 2";

$MESS ["FIELDS_ROWS_TITLE"] = "Состав заявки";
$MESS ["FIELDS_ROWS_ROW_BRAND"] = "Бренд";
$MESS ["FIELDS_ROWS_ROW_NAME"] = "Наименование";
$MESS ["FIELDS_ROWS_ROW_COUNT"] = "Количество";
$MESS ["FIELDS_ROWS_ROW_PACKING"] = "Фасовка";
$MESS ["FIELDS_ROWS_ROW_CLIENT"] = "Клиент";

$MESS ["FIELDS_FILE_TITLE"] = "Файл";

$MESS ["FIELDS_COMMENT_TITLE"] = "Комментарий";

$MESS["ROW_BRAND_1"] = 'Бренд 1';
$MESS["ROW_BRAND_2"] = 'Бренд 2';
$MESS["ROW_BRAND_3"] = 'Бренд 3';

$MESS["MAIL_SUBJECT"] = "Новая заявка";